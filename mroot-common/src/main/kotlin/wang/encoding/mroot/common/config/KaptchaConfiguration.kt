package wang.encoding.mroot.common.config


import com.google.code.kaptcha.Constants
import com.google.code.kaptcha.impl.DefaultKaptcha
import com.google.code.kaptcha.util.Config
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import wang.encoding.mroot.common.constant.KaptchaConst
import java.util.*


/**
 * Kaptcha 验证码配置
 *
 * @author ErYang
 */
@Configuration
@EnableCaching
class KaptchaConfiguration {

    @Autowired
    private lateinit var kaptchaConst: KaptchaConst

    @Bean
    fun kaptchaProducer(): DefaultKaptcha {
        val defaultKaptcha = DefaultKaptcha()
        val properties = Properties()
        properties.setProperty(Constants.KAPTCHA_BORDER, kaptchaConst.border)
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_FONT_COLOR, kaptchaConst.textProducerFontColor)
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_FONT_SIZE, kaptchaConst.textProducerFontSize)
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_FONT_NAMES, kaptchaConst.textProducerFontNames)
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_CHAR_LENGTH, kaptchaConst.textProducerCharLength)
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_CHAR_SPACE, kaptchaConst.textProducerCharSpace)
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_CHAR_STRING, kaptchaConst.textProducerCharString)
        properties.setProperty(Constants.KAPTCHA_IMAGE_WIDTH, kaptchaConst.imageWidth)
        properties.setProperty(Constants.KAPTCHA_IMAGE_HEIGHT, kaptchaConst.imageHeight)
        properties.setProperty(Constants.KAPTCHA_BACKGROUND_CLR_FROM, kaptchaConst.backgroundClearFrom)
        properties.setProperty(Constants.KAPTCHA_BACKGROUND_CLR_TO, kaptchaConst.backgroundClearTo)
        properties.setProperty(Constants.KAPTCHA_NOISE_COLOR, kaptchaConst.noiseColor)
        properties.setProperty(Constants.KAPTCHA_NOISE_IMPL, kaptchaConst.noiseImpl)
        properties.setProperty(Constants.KAPTCHA_OBSCURIFICATOR_IMPL, kaptchaConst.obscurificatorImpl)
        properties.setProperty(Constants.KAPTCHA_SESSION_KEY, kaptchaConst.sessionKey)
        properties.setProperty(Constants.KAPTCHA_SESSION_DATE, kaptchaConst.sessionDate)
        val config = Config(properties)
        defaultKaptcha.config = config
        return defaultKaptcha
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End KaptchaConfiguration class

/* End of file KaptchaConfiguration.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/common/config/KaptchaConfiguration.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
