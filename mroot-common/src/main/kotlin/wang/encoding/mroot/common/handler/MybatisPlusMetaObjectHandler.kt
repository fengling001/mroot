/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.handler


import com.baomidou.mybatisplus.mapper.MetaObjectHandler
import org.apache.ibatis.reflection.MetaObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import wang.encoding.mroot.model.enums.StatusEnum
import java.time.Instant
import java.util.*

/**
 * mybatis plus 注入公共字段自动填充
 *
 * @author ErYang
 */
class MybatisPlusMetaObjectHandler : MetaObjectHandler() {

    /**
     * 新增
     *
     * @param metaObject MetaObject
     */
    override fun insertFill(metaObject: MetaObject) {
        if (logger.isDebugEnabled) {
            logger.debug(">>>>>>>>新增的时候公共字段自动填充<<<<<<<<")
        }
        // 新增时间
        val gmtCreate: Any? = metaObject.getValue("gmtCreate")
        if (null == gmtCreate) {
            metaObject.setValue("gmtCreate", Date.from(Instant.now()))
        }
        // 新增状态
        val status: Any? = metaObject.getValue("status")
        if (null == status) {
            metaObject.setValue("status", StatusEnum.NORMAL.key)
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新
     *
     * @param metaObject MetaObject
     */
    override fun updateFill(metaObject: MetaObject) {
        if (logger.isDebugEnabled) {
            logger.debug(">>>>>>>>更新的时候公共字段自动填充<<<<<<<<")
        }
        // 更新时间
        val gmtModified: Any? = metaObject.getValue("gmtModified")
        if (null == gmtModified) {
            metaObject.setValue("gmtModified", Date.from(Instant.now()))
        }
    }

    companion object {

        private val logger: Logger = LoggerFactory.getLogger(MybatisPlusMetaObjectHandler::class.java)
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End MybatisPlusMetaObjectHandler class

/* End of file MybatisPlusMetaObjectHandler.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/common/handler/MybatisPlusMetaObjectHandler.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
