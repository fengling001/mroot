/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------



package ${servicePackageName}.${classPrefix}


import ${modelPackageName}.${classPrefix}.${className}
import wang.encoding.mroot.common.service.BaseService

import java.math.BigInteger

/**
 * 后台 ${classComment} Service 接口
 *
 * @author ErYang
 */
interface ${className}Service : BaseService<${className}> {



    /**
     * 初始化新增 ${className} 对象
     *
     * @param ${classFirstLowerCaseName} ${className}
     * @return ${className}
     */
fun  initSave${className}(${classFirstLowerCaseName}:${className}):${className}

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化修改 ${className} 对象
     *
     * @param ${classFirstLowerCaseName} ${className}
     * @return ${className}
     */
fun initEdit${className}(${classFirstLowerCaseName}:${className}):${className}

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     */
    fun  validation${className}(${classFirstLowerCaseName}:${className}):String?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 查询 ${className}  缓存
     *
     * @param id ID
     * @return ${className}
     */
    fun getById2Cache(id: BigInteger): ${className}?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据标识查询 ${className}
     *
     * @param name 标识
     * @return ${className}
     */
    fun getByName(name: String): ${className}?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据名称查询 ${className}
     *
     * @param title 名称
     * @return ${className}
     */
    fun getByTitle(title: String): ${className}?

    // -------------------------------------------------------------------------------------------------

  /**
     * 新增 ${classComment}
     *
     * @param ${classFirstLowerCaseName} ${className}
     * @return ID  BigInteger
     */
    fun  saveBackId(${classFirstLowerCaseName}:${className}):BigInteger?

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 ${classComment}
     *
     * @param ${classFirstLowerCaseName} ${className}
     * @return ID  BigInteger
     */
   fun  updateBackId(${classFirstLowerCaseName}:${className}):BigInteger?

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 ${classComment} (更新状态)
     *
     * @param id BigInteger
     * @return ID  BigInteger
     */
   fun  removeBackId(id:BigInteger):BigInteger?

    // -------------------------------------------------------------------------------------------------

   /**
     * 恢复 ${classComment} (更新状态)
     *
     * @param id BigInteger
     * @return ID  BigInteger
     */
    fun recoverBackId(id: BigInteger): BigInteger?

    // -------------------------------------------------------------------------------------------------

 /**
     * 根据 id 移除 ${className} 缓存
     *
     * @param id BigInteger
     *
     */
    fun removeCacheById(id: BigInteger)

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据  id 批量移除 ${className} 缓存
     *
     * @param idArray ArrayList<BigInteger>
    *
    */
    fun removeBatchCacheById(idArray: ArrayList<BigInteger>)

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ${className}Service interface

/* End of file ${className}Service.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/service/${classPrefix}/${className}.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
