<#-- 表单验证国际化提示信息 -->
<script>

    /**
     * ${classComment}提示信息
     */
    var ${className}Validation = function () {

        <#list generateModels as var>
            <#if "id" != var.camelCaseName && "type" != var.camelCaseName &&
            "gmtCreate" != var.camelCaseName && "gmtCreateIp" != var.camelCaseName
            && "gmtModified" != var.camelCaseName
            && "sort" != var.camelCaseName && "remark" != var.camelCaseName
            && "status" != var.camelCaseName && "name" !=var.camelCaseName && "title" !=var.camelCaseName>
var ${var.camelCaseName}_required = '${r"${"}I18N("jquery.validation.${classPrefix}.${classFirstLowerCaseName}.${var.camelCaseName}.required")${r"}"}';
            </#if>
            <#if "type" == var.camelCaseName >
             var type_range = '${r"${"}I18N("jquery.validation.type.range")${r"}"}';
            </#if>
            <#if "name" == var.camelCaseName >
             var name_pattern = '${r"${"}I18N("jquery.validation.name.pattern")${r"}"}';
            </#if>
            <#if "title" == var.camelCaseName >
              var title_pattern = '${r"${"}I18N("jquery.validation.title.pattern")${r"}"}';
            </#if>
            <#if "sort" == var.camelCaseName >
                     var sort_range = '${r"${"}I18N("jquery.validation.sort.range")${r"}"}';
            </#if>
            <#if "status" == var.camelCaseName >
                     var status_range = '${r"${"}I18N("jquery.validation.status.range")${r"}"}';
            </#if>
            <#if "remark" == var.camelCaseName >
              var remark_pattern = '${r"${"}I18N("jquery.validation.remark.pattern")${r"}"}';
            </#if>
        </#list>

    return {

                    <#list generateModels as var>

                        <#if "id" != var.camelCaseName && "type" != var.camelCaseName &&
                        "gmtCreate" != var.camelCaseName && "gmtCreateIp" != var.camelCaseName
                        && "gmtModified" != var.camelCaseName
                        && "sort" != var.camelCaseName && "remark" != var.camelCaseName
                        && "status" != var.camelCaseName && "name" !=var.camelCaseName && "title" !=var.camelCaseName>
                                get${var.camelCaseName}Pattern: function () {
                                    return ${var.camelCaseName}_required;
                                },
                        </#if>
                        <#if "type" == var.camelCaseName >
               getTypeRange: function () {
                   return type_range;
               },
                        </#if>

                        <#if "name" == var.camelCaseName >
                 getNamePattern: function () {
                     return name_pattern;
                 },
                        </#if>

                        <#if "title" == var.camelCaseName >
              getTitlePattern: function () {
                  return title_pattern;
              },
                        </#if>

                        <#if "status" == var.camelCaseName >
                        getStatusRange: function () {
                            return status_range;
                        },
                        </#if>

                        <#if "sort" == var.camelCaseName >
                        getSortRange: function () {
                            return sort_range;
                        },
                        </#if>

                        <#if "remark" == var.camelCaseName >
                getRemarkPattern: function () {
                    return remark_pattern;
                }
                        </#if>


                    </#list>

        // -------------------------------------------------------------------------------------------------

    }
    }();

    // -------------------------------------------------------------------------------------------------

</script>
